Requirements
------------
Drupal 6.x with the taxonomy module enabled.

Installation
------------
This module requires no database modifications.

1. Download and unzip the taxonomy_filter project file in the appropriate site directory
(e.g. in /sites/all/modules or /sites/my_site/modules).
2. In Administer > Site building > Modules (admin/build/modules), enable the Taxonomy Node Filter module.
3. In Administer > Site configuration > Taxonomy filter (admin/settings/taxonomy_node_filter), configure the module as needed.
4. In Administer > Site building > Blocks (admin/build/block), set the Taxonomy Node Filter blocks to display.

Configuration
-------------
Go to configuration page of Taxonomy Node Filter block. Select vocabularies you want to attach.
Select type of search operator 'OR' or 'AND' which will be used on term page
