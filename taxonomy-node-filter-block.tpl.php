<?php

/**
 * @file
 * Default theme implementation for displaying a taxonomy node filter form
 * within a block.
 *
 * Available variables:
 * - $content: Rendered taxonomy node filter form.
 *
 */
?>
<div class="taxonomy-node-filter-block">
<?php print $content; ?>
</div>
